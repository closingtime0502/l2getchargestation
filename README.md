L2GetChargeStation
====

## Description
[アデンチャージステーション 毎日プレゼント](https://www.ncsoft.jp/shop/1949/31911/detail)の受け取り用スクリプトです。
SeleniumのChromeDriveを使って、アイテムを自動で受け取ってくれます。Windowsのタスクスケジューラに登録することで実行も自動化することができます。

## Usage

#### 1. 受け取りたいアカウントの情報を入力する

```py
account_id = ""
password = ""
server_name = ""
character_name = ""
```

#### 2. ChromeDriverをダウンロードする

[ここ](https://chromedriver.chromium.org/downloads)から、自分のChromeのバージョンにあったものをダウンロードし、`l2getchargestation`直下に配置します。

※ デフォルトでは、バージョン76を配置しています。

#### 3. Seleniumをダウンロードする
`pip install selenium`

#### 4. タスクスケジューラに登録する
以下のバッチファイルを実行するように登録します。
`BootFromTaskScheduler.bat`

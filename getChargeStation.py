"""
    @brief   アデンチャージステーションの受け取り用スクリプト
    @author  J
    @date    2019/09/01
"""

from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# ****************************************************
# アカウント
# ****************************************************
account_id = ""
password = ""
server_name = ""
character_name = ""
# ****************************************************
#
# ChromeDriver - WebDriver for Chrome
# https://chromedriver.chromium.org/downloads
#
# [for Windows]
# driver = webdriver.Chrome("./chromedriver.exe")
#
# [for Linux]
driver = webdriver.Chrome("./chromedriver")
# ****************************************************

url = "https://www.ncsoft.jp/shop/1949/31911/detail"

driver.get(url)

wait = WebDriverWait(driver, 30)

try:
    # ログインする
    wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "個別購買"))).click()
    wait.until(EC.presence_of_element_located((By.ID, "account"))).send_keys(account_id)
    wait.until(EC.presence_of_element_located((By.ID, "password"))).send_keys(password)
    wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "ログイン"))).click()

    # ログインダイアログが閉じるのを待って購入する
    wait.until(EC.invisibility_of_element_located((By.ID, "account")))
    wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "個別購買"))).click()

    # 隠れているサーバ・キャラ選択のselectboxを表示させる
    wait.until(EC.presence_of_element_located((By.ID, "server_32")))
    driver.execute_script("document.getElementById('server_32').style.display = 'block';")
    wait.until(EC.presence_of_element_located((By.ID, "character_32")))
    driver.execute_script("document.getElementById('character_32').style.display = 'block';")

    # サーバとキャラを選択する
    server = driver.find_element_by_id('server_32')
    Select(server).select_by_visible_text(server_name)
    character = driver.find_element_by_id('character_32')
    Select(character).select_by_visible_text(character_name)
    driver.find_element_by_link_text("次へ").click()

    # 購入する
    wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "購入する"))).click()
    wait.until(EC.presence_of_element_located((By.ID, "popInputPasswordTxt"))).send_keys(password)
    wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "確認"))).click()

    # ログアウトする
    wait.until(EC.invisibility_of_element_located((By.XPATH, "/html/body/div[33]")))
    driver.find_element_by_xpath('//*[@id="myPlaync"]/div/div[1]/div/ul/li[3]/a').click()

# 起動しっぱなしを防ぐために落とす
finally:
    driver.implicitly_wait(5)
    driver.quit()
